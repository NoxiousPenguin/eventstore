# Event Store

An open-source and functional database in Java.


## Building the Application:

Make sure to install Git and clone the repo using 
'''
git clone https://gitlab.com/mwillema/eventstore.git
'''

## Contributions
Any contribution to the app is welcome in the form of pull requests.

## License and copyright infringements
I will consider any kind of license or copyright infringements seriously and will send copyright claim notice or license infringement notice to anyone who is not adhering to them.


## Authors

* **Marco Willemart @mwillema**

## License

This project is licensed under the  Apache License Version 2.0, more information cn be found [here](https://choosealicense.com/licenses/apache-2.0/)
